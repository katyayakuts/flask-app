import os
import json
import urllib.request

TOKEN = os.getenv("TOKEN")
HEADERS = {'Authorization': f'Bearer {TOKEN}'}


def get_pull_requests(state):
    """
    Example of return:
    [
        {"title": "Add useful stuff", "num": 56, "link": "https://github.com/boto/boto3/pull/56"},
        {"title": "Fix something", "num": 57, "link": "https://github.com/boto/boto3/pull/57"},
    ]
    """

    url = 'https://api.github.com/repos/boto/boto3/pulls?state=' + state + '&per_page=100'

    response = urllib.request.urlopen(url)

    data = json.loads(response.read())

    answer = []

    if state == 'open' or state == 'closed':
        for mr in data:
            curr = [mr['number'], mr['title'], mr['url']]
            answer.append(curr)
    elif state == 'needs-review':
        for mr in data:
            label = mr['labels']
            for ll in label:
                if ll['name'] == 'needs-review':
                    curr = [mr['number'], mr['title'], mr['url']]
                    answer.append(curr)
    elif state == 'bug':
        for mr in data:
            label = mr['labels']
            for ll in label:
                if ll['name'] == 'bug':
                    curr = [mr['number'], mr['title'], mr['url']]
                    answer.append(curr)

    return answer
